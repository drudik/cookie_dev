/**

Нужно реализовать функциональную кнопку,
которая будет работать по принципу кнопки “Start” на микроволновках.
Итерационный шаг равняется 10 секундам.
Красота UI не важна.

*/
import './App.css';
import { useState, useEffect } from "react";

function App() {
  const start = 0;
  const incrementor = 10;
  const [startItem, setStartItem] = useState(start);

  const addTime = () => {
    setStartItem(startItem + incrementor);
  };
 
  useEffect(() => {
    if (startItem > 0) {
      const timer = setTimeout(() => {
        setStartItem(startItem - 1);
      }, 1000);
      return () => {clearTimeout(timer)};
    } 
  }, [startItem]);

  return (
    <div className="App">
      <h1>Microwave oven</h1>

      <h2>Start editing to see some magic happen!</h2>

      <div>
        <div className="microwave">it is microwave</div> 
        <p>{startItem}</p>
        <button onClick={addTime}>Add time</button>
      </div>
    </div>
  );
}

export default App;
